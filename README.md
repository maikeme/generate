Go package inspired by icza's great [SO answer](https://stackoverflow.com/questions/22892120/how-to-generate-a-random-string-of-a-fixed-length-in-golang). I made slight modification to include numbers and omit characters that can be difficult to differentiate (such as lI1).

Get:
```
go get -u gitlab.com/maikeme/generate
```

Use:
```
package main

import (
    "gitlab.com/maikeme/generate"
    "fmt"
)

func main() {
	rand6CharString := generate.RandomString(6)
	rand100CharString := generate.RandomString(100)
    fmt.Println(rand6CharString)
    fmt.Println(rand100CharString)
}
```

The package has intentionally been named `generate` as I may include more useful functions in the future.