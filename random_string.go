package generate

import (
	"math/rand"
	"time"
)

const (
	letterBytes   = "abcdefghkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789"
	letterIdxBits = 6
	letterIdxMask = 1<<letterIdxBits - 1
	letterIdxMax  = 63 / letterIdxBits
)

var randUnixNanoSrc = rand.NewSource(time.Now().UnixNano())

func RandomString(n int) string {
	b := make([]byte, n)
	for i, cache, remain := n-1, randUnixNanoSrc.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = randUnixNanoSrc.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}
	return string(b)
}
